
import * as vscode from "vscode";
import * as API from "./generated/api";
import { LOGGER } from "./helper/logger";

import { StatusBar } from "./helper/status-bar";
import { TeamCity } from "./helper/teamcity";


interface IRequestError {
  code: string | "ENOTFOUND";
  errno: number;
  hostname: string;
  syscall: string;
  message: string;
  stack: string;
}


export class Watcher {

  private intervalId: NodeJS.Timeout | undefined;

  private readonly statusBarHelper: StatusBar;
  private readonly teamcityHelper: TeamCity;


  constructor(
    readonly serverUrl: string,
    readonly authenticationToken: string,
    private readonly updateInterval: number,
    readonly trackId: string
  ) {
    this.statusBarHelper = new StatusBar();
    this.teamcityHelper = new TeamCity(
      serverUrl,
      authenticationToken,
      trackId
    );

    vscode.commands.registerCommand("extension.showBuildInformation", (info, url, action) => {
      vscode.window.showErrorMessage(info, action).then((act) => {

        if (act !== "Show in web") {
          return;
        }

        if (!url) {
          return;
        }

        vscode.env.openExternal(url);
      });
    });
  }


  start(): void {
    this.intervalId = setInterval(async () => {
      this.update();
    }, this.updateInterval);

    this.update();
    
    this.statusBarHelper.show();
  }


  stop(): void {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }

    this.statusBarHelper.hide();
  }


  private async update(): Promise<void> {

    LOGGER.info("Fetch update");

    try {
      if (await this.checkRunningsBuilds()) {
        return;
      }

      await this.checkLastFinishedBuild();
    } catch (error) {

      if (this.isRequestError(error) && error.code === "ENOTFOUND") {
        this.statusBarHelper.showWarningMessage("TeamCity: Not reachable");
      }

      console.error(error);
    }
  }


  private async checkRunningsBuilds(): Promise<boolean> {
    const runningBuilds = await this.teamcityHelper.getRunningBuilds();

    const isRunningBuildFailing = this.isOneOfTheBuildsFailing(runningBuilds);

    if (isRunningBuildFailing) {
      LOGGER.info(`Running build failing: ${isRunningBuildFailing}`);
      const buildToParse = this.getFirstFailingBuild(runningBuilds);
      this.statusBarHelper.showErrorMessage(
        "TeamCity: Running build is failing $(thumbsdown)",
        await this.generateCommand(buildToParse as API.Build)
      );
    }

    return isRunningBuildFailing;
  }


  private async checkLastFinishedBuild() {

    const lastFinishedBuild = await this.teamcityHelper.getLastFinishedBuild();

    if (!lastFinishedBuild) {
      return;
    }

    const isLastFinishedBuildFailing = this.isTheBuildFailing(lastFinishedBuild);

    if (isLastFinishedBuildFailing) {
      LOGGER.info(`Last finished build is failing: ${isLastFinishedBuildFailing}`);
      this.statusBarHelper.showErrorMessage(
        "TeamCity: Last build failed $(thumbsdown)",
        await this.generateCommand(lastFinishedBuild)
      );
    } else {
      LOGGER.info(`Last finished build is successful`);
      this.statusBarHelper.showInfoMessage("TeamCity: Last build successful $(thumbsup)");
    }
  }


  private isOneOfTheBuildsFailing(builds: API.Builds | undefined): boolean {
    if (!builds) {
      return false;
    }
    return builds.build?.filter((build) => !build.personal)
      .some((build) => build.status === "FAILURE") || false;
  }


  private isTheBuildFailing(build: API.Build | undefined): boolean {
    if (!build) {
      return false;
    }
    return build.status === "FAILURE";
  }


  private getFirstFailingBuild(builds: API.Builds | undefined): API.Build | undefined {
    return builds?.build?.find((build) => build.status === "FAILURE");
  }


  private async generateCommand(build: API.Build) {
    return {
      command: "extension.showBuildInformation",
      arguments: [
        await this.generateMessage(build),
        vscode.Uri.parse(build.webUrl as string),
        "Show in web"
      ],
      title: "Show build information"
    } as vscode.Command
  }


  private async generateMessage(build: API.Build) {
    const changes = await this.teamcityHelper.getChanges(build.id as number);

    if (changes?.change) {
      const parsedChanges = await this.getFormattedMessage(changes.change);

      const tooltip = [
        build.buildTypeId,
        build.status,
        parsedChanges
          .filter((value) => !!value)
          .join(" - ")
      ];

      return tooltip.join(" -- ");
    }

    return "";
  }


  private getFormattedMessage(changes: API.Change[]): Promise<string[]> {
    return Promise.all(changes?.map(async (c) => {

      if (!c.id) {
        return "";
      }

      const change = await this.teamcityHelper.getChange(c.id);

      if (!change) {
        return "";
      }

      return `${change.version} ${change.username} ${change.comment}`;
    }));
  }


  private isRequestError(error: unknown): error is IRequestError {
    if (!error || typeof error !== "object") {
      return false;
    }
    return error.hasOwnProperty("code") &&
      error.hasOwnProperty("errno") &&
      error.hasOwnProperty("hostname") &&
      error.hasOwnProperty("syscall") &&
      error.hasOwnProperty("message") &&
      error.hasOwnProperty("stack");
  }

}