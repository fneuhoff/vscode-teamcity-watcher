
import * as vscode from "vscode";


export class StatusBar {

  private statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 5);


  show() {
    this.statusBarItem.show();
  }


  hide() {
    this.statusBarItem.dispose();
    this.statusBarItem.hide();
  }


  showInfoMessage(message: string) {
    this.updateStatusBar(message);
  }


  showErrorMessage(message: string, command: vscode.Command) {
    this.updateStatusBar(
      message,
      new vscode.ThemeColor("statusBarItem.errorBackground"),
      command
    );
  }


  showWarningMessage(message: string) {
    this.updateStatusBar(
      message,
      new vscode.ThemeColor("statusBarItem.warningBackground")
    );
  }


  private updateStatusBar(
    message: string,
    color: vscode.ThemeColor | undefined = undefined,
    command: vscode.Command | undefined = undefined
  ) {
    this.statusBarItem.text = message;
    this.statusBarItem.backgroundColor = color;
    this.statusBarItem.command = command;
  }

}