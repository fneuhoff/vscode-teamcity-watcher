
import * as API from "../generated/api";
import { LOGGER } from "./logger";


export class TeamCity {

  private readonly buildApi: API.BuildApi;
  private readonly changeApi: API.ChangeApi;

  constructor(
    readonly serverUrl: string,
    readonly authenticationToken: string,
    private readonly trackId: string
  ) {
    this.buildApi = new API.BuildApi(serverUrl);
    this.changeApi = new API.ChangeApi(serverUrl);

    const auth = new API.OAuth();
    auth.accessToken = authenticationToken;

    this.buildApi.setDefaultAuthentication(auth);
    this.buildApi.useStrictSSL = false;
    this.changeApi.setDefaultAuthentication(auth);
    this.changeApi.useStrictSSL = false;
  }

  async getChanges(buildId: number): Promise<API.Changes | undefined> {
    try {
      const response = await this.changeApi.serveChanges(undefined, undefined, `${buildId}`);
      LOGGER.info("Get changes succeeded: ", response.body);
      return response.body;
    } catch (error) {
      LOGGER.error("Get changes failed: ", error);
      throw error;
    }
  }


  async getChange(changeId: number): Promise<API.Change | undefined> {
    try {
      const response = await this.changeApi.serveChange(`id:${changeId}`);
      LOGGER.info("Get change succeeded: ", response.body);
      return response.body;
    } catch (error) {
      LOGGER.error("Get change failed: ", error);
      throw error;
    }
  }

  async getRunningBuilds(): Promise<API.Builds | undefined> {
    try {
      const response = await this.buildApi.serveAllBuilds(
        this.trackId,
        undefined,
        undefined,
        false,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        "running:true"
      );
      LOGGER.info("Get running builds succeeded: ", response.body);
      return response.body;
    } catch (error) {
      LOGGER.error("Get running builds failed: ", error);
      throw error;
    }
  }


  async getLastFinishedBuild(): Promise<API.Build | undefined> {
    try {
      const response = await this.buildApi.serveBuild(`buildType(id:${this.trackId}),count:1,running:false,personal:false`);
      LOGGER.info("Get last finished build succeeded: ", response.body);
      return response.body;
    } catch (error) {
      LOGGER.error("Get last finished failed: ", error);
      throw error;
    }
  }

}