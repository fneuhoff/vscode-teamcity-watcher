
import * as vscode from "vscode";


class Logger {

  private outputChannel: vscode.OutputChannel;


  constructor() {
    this.outputChannel = vscode.window.createOutputChannel("TeamCity status watcher");
  }


  error(message: string, error: unknown): void {
    this.info(message, error);

    vscode.window.showWarningMessage("An error occurred in TeamCity watcher extension.", "Show output")
      .then((act) => {
        if (act !== "Show output") {
          return;
        }

        this.outputChannel.show();
      });
  }


  info(message: string, value: unknown = undefined): void {
    if (value) {
      this.outputChannel.appendLine(`${message} ${JSON.stringify(value, null, 2)}`);
    } else {
      this.outputChannel.appendLine(message);
    }
  }

}


export const LOGGER = new Logger();